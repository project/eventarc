<?php

namespace Drupal\eventarc\Controller;

use Drupal\eventarc\Event\EventarcEvent;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use CloudEvents\Serializers\JsonDeserializer;
use Firebase\JWT\JWT;
use Firebase\JWT\JWK;
use GuzzleHttp\ClientInterface;

/**
 * Class EventarcController.
 */
class EventarcController extends ControllerBase {

  const JWT_ALG = 'RS256';

  private static $binaryAttributes = [
    'id',
    'source',
    'specversion',
    'type',
    'dataschema',
    'subject',
    'time',
  ];

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Drupal cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The HTTP client used to fetch Google's JWKs.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Used to record watchdog logs.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The config object for eventarc.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new EventarcController.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Drupal cache backend.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP client used to fetch Google's JWKs.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The Drupal config factory to get this module's settings.
   * @param \Psr\Log\LoggerInterface $logger
   *   The watchdog log.
   */
  public function __construct(
    EventDispatcherInterface $event_dispatcher,
    CacheBackendInterface $cache,
    ClientInterface $http_client,
    ImmutableConfig $config,
    LoggerInterface $logger) {
    $this->eventDispatcher = $event_dispatcher;
    $this->cache = $cache;
    $this->httpClient = $http_client;
    $this->config = $config;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('cache.default'),
      $container->get('http_client'),
      $container->get('config.factory')->get('eventarc.settings'),
      $container->get('logger.factory')->get('eventarc'),
    );
  }

  /**
   * Receive an event from Google's eventarc and dispatch using Symfony Events.
   */
  public function receive(Request $request) {
    // From https://github.com/GoogleCloudPlatform/functions-framework-php/blob/main/src/CloudEventFunctionWrapper.php#L126
    // get the eventarc headers and store them as cloudevent attributes.
    $attributes = [];
    foreach (self::$binaryAttributes as $attr) {
      $ceHeader = 'ce-' . $attr;
      if ($request->headers->has($ceHeader)) {
        $attributes[$attr] = $request->headers->get($ceHeader);
      }
    }
    $contentType = $request->headers->get('content-type');

    // Deserialize the payload from eventarc's POST request.
    $data = $request->getContent();
    $cloudEvent = JsonDeserializer::create()->deserializeBinary($data, $contentType, $attributes);
    $data = $cloudEvent->getData();

    // Dispatch the event within Drupal's Symfony Events.
    $event = new EventarcEvent($data);
    // Get the CloudEvent type and use that as the event to dispatch.
    $event_type = $attributes['type'];
    $this->eventDispatcher->dispatch($event, $event_type);

    $response = new Response(
      'OK',
      Response::HTTP_OK,
      ['content-type' => 'text/plain']
    );

    return $response;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account. This won't be used and insted we'll look at the Authorization header.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $jwt = '';
    $auth = \Drupal::request()->headers->get('Authorization');
    if ($auth) {
      $matches = [];
      if (preg_match('/^Bearer (.+)/', $auth, $matches)) {
        $jwt = $matches[1];
      }
    }

    if ($jwt === '') {
      $this->logger->error("No token found in Authorization header");
      return AccessResult::forbidden();
    }

    try {
      $googleJWKs = $this->getGoogleJWKs();
      $decoded = JWT::decode($jwt, JWK::parseKeySet($googleJWKs), [self::JWT_ALG]);
      $allow_accounts = $this->config->get('service_accounts');
      if (!is_array($allow_accounts)) {
        $this->logger->error("No service accounts allowed to publish events.");
        return AccessResult::forbidden();
      }

      $this->logger->info(t('Received request from @email', ['@email' => $decoded->email]));

      return AccessResult::allowedIf(in_array($decoded->email, $allow_accounts));
    }
    catch (Exception $e) {
      $this->logger->error("Failed to retrieve google's public key");
    }

    return AccessResult::forbidden();
  }

  /**
   *
   */
  public function getGoogleJWKs() {
    $publicKeys = [];
    $cache_id = 'eventarc.google_jwks';
    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }
    else {
      $discoveryUrl = 'https://accounts.google.com/.well-known/openid-configuration';
      $response = $this->httpClient->request('GET', $discoveryUrl, ['retry_enabled' => TRUE]);
      $discovery = json_decode($response->getBody(), TRUE);

      $publicKeysUrl = $discovery['jwks_uri'];
      $response = $this->httpClient->request('GET', $publicKeysUrl, ['retry_enabled' => TRUE]);
      $publicKeys = json_decode($response->getBody(), TRUE);

      if ($publicKeys) {
        // Only cache Google's public keys for one hour.
        $cache_expire = time() + 3600;
        $this->cache->set($cache_id, $publicKeys, $cache_expire);
      }
      else {
        $this->logger->error("Failed to retrieve google's public key");
      }
    }
    return $publicKeys;
  }

}
