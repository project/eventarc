<?php

namespace Drupal\eventarc\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Wraps an Eventarc event for event subscribers.
 *
 * @ingroup eventarc
 */
class EventarcEvent extends Event {

  /**
   * CloudEvent data.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructs an eventarc event object.
   *
   * @param string $data
   *   The Cloud Event data object.
   */
  public function __construct($data) {
    $this->data = $data;
  }

  /**
   *
   */
  public function getData() {
    return $this->data;
  }

}
