<?php

namespace Drupal\eventarc\Event;

/**
 *
 */
final class CloudStorageEvents {

  /**
   * Name of the event fired when an Google Cloud Storage (GCS) object is written to in GCS.
   *
   * The event listener method receives a  \Drupal\eventarc\Event\EventarcEvent instance.
   *
   * @Event
   *
   * @see \Drupal\eventarc\Event\EventarcEvent
   * @see https://cloud.google.com/eventarc/docs/reference/supported-events#cloud-storage
   *
   * @var string
   */
  const FINALIZED = 'google.cloud.storage.object.v1.finalized';

  /**
   * Name of the event fired when an Google Cloud Storage (GCS) object is deleted from GCS.
   *
   * The event listener method receives a \Drupal\eventarc\Event\EventarcEvent instance.
   *
   * @Event
   *
   * @see \Drupal\eventarc\Event\EventarcEvent
   * @see https://cloud.google.com/eventarc/docs/reference/supported-events#cloud-storage
   *
   * @var string
   */
  const DELETED = 'google.cloud.storage.object.v1.deleted';

}
