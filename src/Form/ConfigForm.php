<?php

namespace Drupal\eventarc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'eventarc.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eventarc.settings');
    $service_accounts = $config->get('service_accounts', []);

    // Gather the number of names in the form already.
    $num_gsas = $form_state->get('num_gsas');
    // We have to ensure that there is at least one name field.
    if ($num_gsas === NULL) {
      $num_gsas = $service_accounts ? count($service_accounts) : 1;
      $name_field = $form_state->set('num_gsas', $num_gsas);
    }

    $form['#tree'] = TRUE;
    $form['service_accounts'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Google Service Accounts that can publish events to this site'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_gsas; $i++) {
      $form['service_accounts']['email'][$i] = [
        '#type' => 'email',
        '#title' => $this->t('Service Account'),
        '#default_value' => empty($service_accounts[$i]) ? '' : $service_accounts[$i],
      ];
    }

    $form['service_accounts']['actions'] = [
      '#type' => 'actions',
    ];
    $form['service_accounts']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another GSA'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];
    // If there is more than one name, add the remove button.
    if ($num_gsas > 1) {
      $form['service_accounts']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove last GSA'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['service_accounts'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_gsas');
    $add_button = $name_field + 1;
    $form_state->set('num_gsas', $add_button);
    // Since our buildForm() method relies on the value of 'num_gsas' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_gsas');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_gsas', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_gsas' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Final submit handler.
   *
   * Reports what values were finally set.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $emails = $form_state->getValue(['service_accounts', 'email']);

    parent::submitForm($form, $form_state);

    $this->config('eventarc.settings')
      ->set('service_accounts', $emails)
      ->save();
  }

}
