<?php

namespace Drupal\eventarc\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the eventarc module.
 */
class EventarcControllerTest extends WebTestBase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "eventarc EventarcController's controller functionality",
      'description' => 'Test Unit for module eventarc and controller EventarcController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests eventarc functionality.
   */
  public function testEventarcController() {
    // Check that the basic functions of module eventarc.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
